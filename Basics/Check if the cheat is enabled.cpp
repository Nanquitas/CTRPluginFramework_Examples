#include "CTRPluginFramework.hpp"

/*
 * In this example we'll see how to create a cheat entry and check if the cheat is activated or not inside the function
 */

namespace CTRPluginFramework
{
    /**
     * @brief      My cheat function
     *
     * @param      entry: A pointer to it's MenuEntry object
     */
    void    NoCooldown(MenuEntry *entry)
    {
        // If the entry just got activated, I patch the process to disable the cooldown
        if (entry->WasJustActivated())
        {
            Process::Write32(0x058799A, 0xE320F000);
        }
        // Else if the cheat is disabled, I put back the original data
        else if (entry->IsActivated() == false)
        {
            Process::Write32(0x058799A, 0xE0401001);
        }
        
    }

    void   main(void)
    {
        // We create our PluginMenu object
        PluginMenu  *menu = new PluginMenu("Example");

        // We create our MenuEntry object
        // Arguments: Name, function and note
        MenuEntry   *entry = new MenuEntry("Disable cooldown", NoCooldown);

        // We add our entry to the menu
        menu->Append(entry);

        // We launch our menu
        menu->Run();
    }
}
