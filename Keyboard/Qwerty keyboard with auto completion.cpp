#include "CTRPluginFramework.hpp"
#include <cctype>
#include <string>
#include <vector>
/*
 * In this example we'll see how to create a Qwerty Keyboard 
 * and to auto complete the User's input when the input 
 * correspond a preset list of possibilities
 */

namespace CTRPluginFramework
{
    using StringVector = std::vector<std::string>;
    using StringIter = std::string::iterator;
    using StringConstIter = std::string::const_iterator;

    // Create our list of possibilities
    static const StringVector     g_possibilities = 
    {
        "Bulbasaur",
        "Ivysaur",
        "Venusaur",
        "Charmander",
        "Charmeleon",
        "Charizard",
        "Squirtle",
        "Wartortle",
        "Blastoise",
        "Caterpie",
        "Metapod",
        "Butterfree",
        "Weedle",
        "Kakuna",
        "Beedrill",
        "Pidgey",
        "Pidgeotto",
        "Pidgeot",
        "Rattata",
        "Raticate",
        "Spearow",
        "Fearow",
        "Ekans",
        "Arbok",
        "Pikachu",
        "Raichu"
    };

    /**
     * @brief      Gets the Pokemons that matches the first letters of the input
     *
     * @param      output  The vector where to put the pokemons that matches the input
     * @param      input   The input string entered by the user
     *
     * @return     The amount of pokemons that matches the input.
     */
    int     GetMatches(StringVector &output, std::string &input)
    {
        // Clear the output
        output.clear();

        // Clone the input string but with forced lowcase
        std::string     lowcaseInput(input);

        for (char &c : lowcaseInput)
            c = std::tolower(c);

        // Parse our possibilities to find the matches
        for (const std::string &pokemon : g_possibilities)
        {
            StringIter      inputIt = lowcaseInput.begin();
            StringConstIter pokeIt = pokemon.begin();

            // Parse every letter of input while it matches the pokemon's name
            while (inputIt != lowcaseInput.end() && pokeIt != pokemon.end() && *inputIt == std::tolower(*pokeIt))
            {
                ++inputIt;
                ++pokeIt;
            }

            // If we're at the end of input then it matches the pokemon's name
            if (inputIt == lowcaseInput.end())
                output.push_back(pokemon);
        }

        return (output.size());
    }

    /**
     * @brief      This function will be called by the Keyboard everytime the input change
     *
     * @param      keyboard     The keyboard that called the function
     * @param      event        The event that changed the input
     */
    void    OnInputChange(Keyboard &keyboard, InputChangeEvent &event)
    {
        std::string  &input = keyboard.GetInput();

        // If the user removed a letter do nothing
        if (event.type == InputChangeEvent::CharacterRemoved)
        {
            // Set an error, to avoid that the user can return an incomplete name
            keyboard.SetError("Type a letter to search for a pokemon.");
            return;
        }

        // If input's length is inferior than 3, ask for more letters
        if (input.size() < 3)
        {
            keyboard.SetError("Not enough letters to do the search.\nKeep typing.");
            return;
        }

        // Else do the search
        StringVector    matches;


        // Search for matches
        int count = GetMatches(matches, input);

        // If we don't have any matches, tell the user
        if (!count)
        {
            keyboard.SetError("Nothing matches your input.\nTry something else.");
            return;
        }

        // If we have only one matches, complete the input
        if (count == 1)
        {
            input = matches[0];
            return;
        }

        // If we have more than 1, but less or equal than 10 matches, ask the user to choose which one
        if (count <= 10)
        {
            // Our new keyboard
            Keyboard    listKeyboard;

            // Populate the keyboard with the matches
            listKeyboard.Populate(matches);

            // User can't abort with B
            listKeyboard.CanAbort(false);

            // Nothing to display on the top screen
            listKeyboard.DisplayTopScreen = false;

            // Display the keyboard and get user choice
            int choice = listKeyboard.Open();

            // Complete the input
            input = matches[choice];
            return;
        }

        // We have too much results, the user must keep typing letters
        keyboard.SetError("Too many results: " + std::to_string(count) + "\nType more letters to narrow the results.");
    }

    /**
     * @brief      Check that the user won't try to return an empty string
     *
     * @param[in]  in     pointer to the input
     * @param      error  reference to the error string
     *
     * @return     true if the input is valid, false otherwise
     */
    bool    CheckInput(const void *in, std::string &error)
    {
        std::string &input = *reinterpret_cast<std::string *>(in);

        // Returned string can't be empty
        if (input.empty())
        {
            error = "Name can't be empty.\nPlease type a name to find.";
            return (false);
        }

        return (true);       
    }

    /**
     * @brief      A cheat function that needs the user to select a pokemon before doing something
     *
     * @param      entry  The entry that called the function
     */
    void    SelectAPokemon(MenuEntry *entry)
    {
        // Store the selected pokemon's name
        std::string  output;

        // Create a keyboard
        Keyboard    keyboard("Which pokemon do you want ?");

        // Pass our OnInputChange function to the keyboard
        keyboard.OnInputChange(OnInputChange);

        // Open the keyboard
        if (keyboard.Open(output) != -1)
        {
            // Display the selected pokemon
            MessageBox("You have selected:\n" + output)();
        }
    }
}