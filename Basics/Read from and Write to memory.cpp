#include "CTRPluginFramework.hpp"

/*
 * In this example we'll see how to read and alter some of the game's data
 */

namespace CTRPluginFramework
{
    void   WriteExamples(MenuEntry *entry)
    {
        // If I want to write a 32bits value
        Process::Write32(0x058799A, 0x140);
        // If I want to write a 16bits value
        Process::Write16(0x058799A, 0x140);
        // If I want to write a 8bits value
        Process::Write8(0x058799A, 255);
    }

    void   ReadExamples(MenuEntry *entry)
    {
        u32     value32;
        u16     value16;
        u8      value8;

        // If I want to read a 32bits value
        Process::Read32(0x058799A, value32);
        // If I want to write a 16bits value
        Process::Read16(0x058799A, value16);
        // If I want to write a 8bits value
        Process::Read8(0x058799A, value8);
    }

    void   ReadAndWriteExamples(MenuEntry *entry)
    {
        u32     value;

        // ReadX function returns if the read was successful or not
        if (Process::Read32(0x058799A, value))
        {
            // Success, we can process the value
            if (value < 0x140)
            {
                Process::Write32(0x058799A, 0x140);
            }
        }
    }
}