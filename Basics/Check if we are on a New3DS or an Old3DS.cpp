#include "CTRPluginFramework.hpp"

#include <string>
/*
 * In this example we'll see how to check if the plugin is running on an old3DS or a new3DS
 */

namespace CTRPluginFramework
{
    /**
     * @brief      My cheat function
     *
     * @param      entry: A pointer to it's MenuEntry object
     */
    void    SuperSpeed(MenuEntry *entry)
    {
        // Check the model of the 3DS, to know which key I must check
        bool    isNew3DS = System::IsNew3DS();

        if (isNew3DS)
        {
            // I can check for ZL & ZR
            if (Controller::IsKeyDown(Key::ZL | Key::ZR))
            {
                Process::Write32(0x058799A, 0xFF);
            }            
        }  
        else
        {
            // I check for L + R + DPADUP
            if (Controller::IsKeyDown(Key::L | Key::R | Key::DPadUp))
            {
                Process::Write32(0x058799A, 0xFF);
            }   
        }      
    }

    void   main(void)
    {
        // We create our PluginMenu object
        PluginMenu  *menu = new PluginMenu("Example");

        // We create our MenuEntry object
        // Arguments: Name, function and note
        MenuEntry   *entry = new MenuEntry("Super Speed", SuperSpeed);

        // We add our entry to the menu
        menu->Append(entry);

        // We launch our menu
        menu->Run();
    }
}
