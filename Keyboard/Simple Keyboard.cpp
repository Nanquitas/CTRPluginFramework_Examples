#include "CTRPluginFramework.hpp"
#include <string>
/*
 * In this example we'll see how to create an entry that ask for a user input
 */

namespace CTRPluginFramework
{
    /**
     * @brief      Set the rupee to the value returned by a keyboard with no hint.
     *
     * @param      entry  The menu entry 
     */
    void    SetRupeeFromKeyboardNoHint(MenuEntry *entry)
    {
        // Only execute the cheat if X is pressed
        if (!Controller::IsKeyPressed(Key::X))
            return;

        // Text to pass to our keyboard to help the user
        std::string  topScreenText = "Rupee modifier:\n";
        topScreenText += "Enter the amount of rupee you want.";

        // Create our keyboard
        Keyboard    keyboard(topScreenText);
        // The variable to get the user's input
        u16         input;

        // Show the keyboard and wait for user choice
        // Open returns -1 if the user canceled the keyboard with B
        // In which case we do nothing
        if (keyboard.Open(input) != -1)
        {
            // Write the value set given by the user
            Process::Write16(0x05879A0, input);
        }        
    }

    /**
     * @brief      Set the rupee to the value returned by a keyboard 
     *             with the current rupee's value as hint.
     * @param      entry  The menu entry 
     */
    void    SetRupeeFromKeyboardWithHint(MenuEntry *entry)
    {
        // Only execute the cheat if X is pressed
        if (!Controller::IsKeyPressed(Key::X))
            return;

        // Text to pass to our keyboard to help the user
        std::string  topScreenText = "Rupee modifier:\n";
        topScreenText += "Enter the amount of rupee you want.";

        // Create our keyboard
        Keyboard    keyboard(topScreenText);
        // The variable to get the user's input
        u16         input;
        // The variable to get the current amount of rupee
        u16         current;

        // Read the current amount of rupee
        // If the function returns false, the address couldn't be read so abort
        if (!Process::Read16(0x05879A0, current))
            return;

        // Show the keyboard and wait for user choice
        // We give the current amount of rupee as hint
        // Open returns -1 if the user canceled the keyboard with B
        // In which case we do nothing
        if (keyboard.Open(input, current) != -1)
        {
            // Write the value set given by the user
            Process::Write16(0x05879A0, input);
        }        
    }
}