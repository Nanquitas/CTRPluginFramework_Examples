#include "CTRPluginFramework.hpp"
#include <string>
/*
 * In this example we'll see how to create an entry that ask for a user input and check the input
 */

namespace CTRPluginFramework
{
    /**
     * @brief      { function_description }
     *
     * @param[in]  in     A pointer to the input, must be casted to the appropriate type
     * @param      error  The error text you want to display in error case
     *
     * @return     Return if the input is valid or not (true / false)
     */
    bool    CheckRupeeInput(const void *in, std::string &error)
    {
        // We cast the input into the appropriate type
        // The type is the same as the one we'll pass to the keyboard
        u16 input = *static_cast<const u16 *>(in);

        // In Zelda the maximum rupee value is 999
        // So we check that the user won't put a number bigger than 99
        if (input > 999)
        {
            // We set the error string to help the user
            error = "Error:\nYou can't have more than 999 rupee.\n";
            error += "So enter a value within the range of 0-999, thanks.";

            // We return to the keyboard that the calue is incorrect
            return (false);
        }
        // Value is inferior to 999 so we return to the keyboard that the value is correct
        return (true);
    }

    /**
     * @brief      Set the rupee to the value returned by a keyboard with no hint.
     *
     * @param      entry  The menu entry 
     */
    void    SetRupeeFromKeyboardNoHint(MenuEntry *entry)
    {
        // Only execute the cheat if X is pressed
        if (!Controller::IsKeyPressed(Key::X))
            return;

        // Text to pass to our keyboard to help the user
        std::string  topScreenText = "Rupee modifier:\n";
        topScreenText += "Enter the amount of rupee you want.";

        // Create our keyboard
        Keyboard    keyboard(topScreenText);
        // The variable to get the user's input
        u16         input;

        // Set our keyboard as decimal, not hexadecimal
        keyboard.IsHexadecimal(false);

        // Give our check function to the keyboard
        keyboard.SetCompareCallback(CheckRupeeInput);

        // Show the keyboard and wait for user choice
        // Open returns -1 if the user canceled the keyboard with B
        // In which case we do nothing
        if (keyboard.Open(input) != -1)
        {
            // Write the value given by the user
            Process::Write16(0x05879A0, input);
        }        
    }

    /**
     * @brief      Set the rupee to the value returned by a keyboard 
     *             with the current rupee's value as hint.
     * @param      entry  The menu entry 
     */
    void    SetRupeeFromKeyboardWithHint(MenuEntry *entry)
    {
        // Only execute the cheat if X is pressed
        if (!Controller::IsKeyPressed(Key::X))
            return;

        // Text to pass to our keyboard to help the user
        std::string  topScreenText = "Rupee modifier:\n";
        topScreenText += "Enter the amount of rupee you want.";

        // Create our keyboard
        Keyboard    keyboard(topScreenText);
        // The variable to get the user's input
        u16         input;
        // The variable to get the current amount of rupee
        u16         current;

        // Read the current amount of rupee
        // If the function returns false, the address couldn't be read so abort
        if (!Process::Read16(0x05879A0, current))
            return;

        // Set our keyboard as decimal, not hexadecimal
        keyboard.IsHexadecimal(false);

        // Give our check function to the keyboard
        keyboard.SetCompareCallback(CheckRupeeInput);

        // Show the keyboard and wait for user choice
        // We give the current amount of rupee as hint
        // Open returns -1 if the user canceled the keyboard with B
        // In which case we do nothing
        if (keyboard.Open(input, current) != -1)
        {
            // Write the value set given by the user
            Process::Write16(0x05879A0, input);
        }        
    }
}