#include "CTRPluginFramework.hpp"

/*
 * In this example we'll see how to create a folder and use them to create some category in our menu
 */

namespace CTRPluginFramework
{
    /**
    ** A bunch of hypothetical cheats functions
    **/
    void    UnlockAllHearts(MenuEntry *entry);
    void    UnlockMagic(MenuEntry *entry);
    void    UnlockAllItems(MenuEntry *entry);
    void    PNJAreFunny(MenuEntry *entry);

    void    main(void)
    {
        // We create our PluginMenu object
        PluginMenu  *menu = new PluginMenu("Example");

        // We create our folders
        MenuFolder  *battleFolder = new MenuFolder("Battle codes");
        MenuFolder  *magicFolder = new MenuFolder("Magic codes");
        MenuFolder  *inventoryFolder = new MenuFolder("Inventory codes");
        MenuFolder  *miscFolder = new MenuFolder("Misc. codes");

        // We create our MenuEntry objects and we add them to the corresponding folder
        battleFolder->Append(new MenuEntry("Unlock all hearts", UnlockAllHearts));

        magicFolder->Append(new MenuEntry("Unlock magic", UnlockMagic));

        inventoryFolder->Append(new MenuEntry("Unlock all items", UnlockAllItems));

        miscFolder->Append(new MenuEntry("PNJ are funny", PNJAreFunny));


        // Now, I want the Magic folder inside the Battle codes folder
        // So I add it to the Battle codes folder
        battleFolder->Append(magicFolder);

        // Finally we add our folders to the menu
        menu->Append(battleFolder);
        menu->Append(inventoryFolder);
        menu->Append(PNJAreFunny);

        // We launch our menu
        menu->Run();
    }
}
