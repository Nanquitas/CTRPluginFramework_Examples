#include "CTRPluginFramework.hpp"
#include <string>
#include <vector>
/*
 * In this example we'll see how to create an entry that use a custom keyboard to do something
 */

namespace CTRPluginFramework
{
    /*
     * Hypothetical functions
     */
    void    MakeCoffee(void);
    void    ToastBread(void);
    void    DeliverCola(void);
    void    NukeSomeBuilding(void);

    /**
     * @brief      Let the user bring a "keyboard" with a list of choice according to which you'll execute something
     *
     * @param      entry  The entry
     */
    void    UserChooseWhatToExecute(MenuEntry *entry)
    {
        // Only execute the cheat if X is pressed
        if (!Controller::IsKeyPressed(Key::X))
            return;

        // Text to pass to our keyboard to help the user
        std::string  topScreenText = "Func executer:\n";
        topScreenText += "What do you want to do ?";

        // Create our keyboard
        Keyboard    keyboard(topScreenText);

        // Create our list of choices
        std::vector<std::string> choices =
        {
            "Make my coffee",
            "Start the toaster",
            "Buy me my cola",
            "Nuke my ex's house"
        };

        // Pass our choices list to our keyboard
        keyboard.Populate(keyboard);

        // Show the keyboard and wait for user choice
        // Open returns -1 if the user canceled the keyboard with B
        // In which case we do nothing
        int userchoice = keyboard.Open():

        if (userchoice != -1)
        {
            if (userchoice == 0) MakeCoffee();
            else if (userchoice == 1) ToastBread();
            else if (userchoice == 2) DeliverCola();
            else NukeSomeBuilding();
        }        
    }
}