#include "CTRPluginFramework.hpp"

/*
 * In this example we'll see how to create a cheat entry and add it to the menu
 */

namespace CTRPluginFramework
{
    /**
     * @brief      My cheat function
     *
     * @param      entry: A pointer to it's MenuEntry object
     */
    void    MaxHeart(MenuEntry *entry)
    {
        Process::Write16(0x058799A, 0x140);
    }

    void   main(void)
    {
        // We create our PluginMenu object
        PluginMenu  *menu = new PluginMenu("Example");

        // We create our MenuEntry object
        // Arguments: Name, function and note
        MenuEntry   *entry = new MenuEntry("Unlock all hearts", MaxHeart, "Enable this to unlock all hearts.");

        // We add our entry to the menu
        menu->Append(entry);

        // We launch our menu
        menu->Run();
    }
}
