#include "CTRPluginFramework.hpp"

#include <string>
/*
 * In this example we'll see how to create a cheat entry and change the note which appear on the menu from the cheat function
 */

namespace CTRPluginFramework
{
    /**
     * @brief      My cheat function
     *
     * @param      entry: A pointer to it's MenuEntry object
     */
    void    NoCooldown(MenuEntry *entry)
    {
        // First retrieve the note string
        std::string &note = entry->Note();

        // If the entry just got activated, I patch the process to disable the cooldown
        if (entry->WasJustActivated())
        {
            Process::Write32(0x058799A, 0xE320F000);
            // I change the note
            note = "You've activated this cheat.\n Doing this allows you to use your spells without any cooldown, wherever you are.\nTo get back to a normal gameplay, just disable this cheat.";
        }
        // Else if the cheat is disabled, I put back the original data
        else if (entry->IsActivated() == false)
        {
            Process::Write32(0x058799A, 0xE0401001);
            // I put the original note
            note = "Enable this disable the spell's cooldown.";
        }
        
    }

    void   main(void)
    {
        // We create our PluginMenu object
        PluginMenu  *menu = new PluginMenu("Example");

        // We create our MenuEntry object
        // Arguments: Name, function and note
        MenuEntry   *entry = new MenuEntry("Disable cooldown", NoCooldown, "Enable this disable the spell's cooldown.");

        // We add our entry to the menu
        menu->Append(entry);

        // We launch our menu
        menu->Run();
    }
}
